<?php include('header.php');?>
<?php include('nav.php');?>

<section style="margin:50px 0;margin-top:100px !important;" class="about">

<?php 
        $st = "SELECT * FROM `tbl_content` WHERE `post_type`='page' and `name`='about'";
        $cm = $conn->prepare($st);
        $cm->execute();
        
        while($row = $cm->fetch(PDO::FETCH_ASSOC)){
        ?>
        <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h1 style="text-transform:capitalize;"><?php echo $row['name'];?></h1>
            </div>
        </div>
    
        <div class="row">
            <div class="col-md-6">
                <p>
                   <?php echo $row['content'];?>
                </p>
            </div>
            <div class="col-md-6">
                <img src="<?php echo $row['post_img'];?>" alt="" class="img-fluid">    
            </div>
        </div>
    
    </div>
        <?php
        }
        ?>





</section>

<?php include('footer.php');?>
<?php include('header.php');?>



<link rel="stylesheet" href="css/jquery.fullPage.css">
<link rel="stylesheet" href="css/examples.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

<div id="fullpage">
	<div class="section " id="section0">
        <img src="images/logo.jpg" alt="" style="width:400px;display:block;margin:0 auto;">
		<h1>Welcome!</h1>
		<p>Connect with your family and friends through
travelling and creating sweet memories!!</p>
		<!-- <img src="imgs/fullPage.png" alt="fullPage" /> -->
	</div>
	<div class="section" id="section1">
		

        <?php 
        $st = "SELECT * FROM `tbl_content` WHERE `post_type`='page' and `name`='about'";
        $cm = $conn->prepare($st);
        $cm->execute();
        
        while($row = $cm->fetch(PDO::FETCH_ASSOC)){
        ?>
        <div class="intro">
			<!-- <img src="imgs/1.png" alt="Cool" /> -->
			<h1><?php echo $row['name'];?></h1>
			<p><?php echo custom_echo($row['content'], 200);;?></p>
            <a href="about.php" class="btn btn-primary btn-lg"> Read More.</a>
			
		</div>
        <?php
        }
        ?>


	</div>
	<div class="section" id="section2">
		
    <div class="slide" id="slide0">
            <div class="container">
            <h1>Testimonials</h1>
            
            </div>
        </div>


        
        <?php 
        $st = "SELECT * FROM `tbl_content` WHERE `post_type`='testimonial' ";
        $cm = $conn->prepare($st);
        $cm->execute();
        
        while($row = $cm->fetch(PDO::FETCH_ASSOC)){
        ?>
        <div class="slide" id="slide1">
        <div class="container">
        <h1><?php echo $row['name'];?></h1>
        <p class="" style="font-size:1.5em;"><?php echo $row['content'];?></p>
        </div>
        </div>
        <?php
        }
        ?>
        
    

	</div>

    <div class="section" id="section3">

        
    <div class="intro container xwow">
    <i class="fa fa-map-marker-alt"></i> 29 Aguinaldo street centro 6
Tuguegarao City, Cagayan<br>
<a href="contact.php" class="btn btn-lg">Contact Us</a>
		
		</div>
	</div>

</div>

<style>
.section2 p {font-size:1.5em;}
.xwow{display: block;
    clear: both;
    font-size: 30px;}
</style>




<?php include('footer.php');?>